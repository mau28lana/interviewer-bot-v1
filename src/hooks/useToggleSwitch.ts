export function toggleSwitch(value: boolean): boolean {
  return !value;
}
